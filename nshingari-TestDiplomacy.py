#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_eval, diplomacy_solve

# -----------
# Testdiplomacy
# -----------

class Testdiplomacy (TestCase):
    # ----
    # eval
    # ----
    def test_eval_1(self):
        army = {'A': {'start': 'Madrid', 'command': 'Hold', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'C': {'start': 'London', 'command': 'Support', 'support_target': 'B', 'num_support': 0, 'end': None}}
        end = {'A': {'start': 'Madrid', 'command': 'Hold', 'loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 1, 'end': 'Madrid'},
        'C': {'start': 'London', 'command': 'Support', 'support_target': 'B', 'num_support': 0, 'end': 'London'}}
        self.assertEqual(diplomacy_eval(army), end)

    def test_eval_2(self):
        army = {'A': {'start': 'Madrid', 'command': 'Hold', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'C': {'start': 'London', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'D': {'start': 'Paris', 'command': 'Support', 'support_target': 'B', 'num_support': 0, 'end': None},
        'E': {'start': 'Dublin', 'command': 'support', 'support_target': 'D', 'num_support': 0, 'end': None},
        'F': {'start': 'Dallas', 'command': 'move', 'loc_target': 'Paris', 'num_support': 0, 'end': None}}
        end = {'A': {'start': 'Madrid', 'command': 'Hold','loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'C': {'start': 'London', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'D': {'start': 'Paris', 'command': 'Support', 'support_target': 'B', 'num_support': 1, 'end': 'Paris'},
        'E': {'start': 'Dublin', 'command': 'support', 'support_target': 'D', 'num_support': 0, 'end': 'Dublin'},
        'F': {'start': 'Dallas', 'command': 'move', 'loc_target': 'Paris', 'num_support': 0, 'end': '[dead]'}}
        self.assertEqual(diplomacy_eval(army), end)

    def test_eval_3(self):
        army = {'A': {'start': 'Madrid', 'command': 'Hold', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'C': {'start': 'London', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': None},
        'D': {'start': 'Paris', 'command': 'Support', 'support_target': 'B', 'num_support': 0, 'end': None},
        'E': {'start': 'Dublin', 'command': 'support', 'support_target': 'D', 'num_support': 0, 'end': None}}
        end = {'A': {'start': 'Madrid', 'command': 'Hold', 'loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'B': {'start': 'Barcelona', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 1, 'end': 'Madrid'},
        'C': {'start': 'London', 'command': 'Move', 'loc_target': 'Madrid', 'num_support': 0, 'end': '[dead]'},
        'D': {'start': 'Paris', 'command': 'Support', 'support_target': 'B', 'num_support': 1, 'end': 'Paris'},
        'E': {'start': 'Dublin', 'command': 'support', 'support_target': 'D', 'num_support': 0, 'end': 'Dublin'}}
        self.assertEqual(diplomacy_eval(army), end)

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin support D\nF Dallas move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
