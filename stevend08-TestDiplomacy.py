#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ['A', 'Madrid', 'Hold'])

    def test_read_2(self):
        s = "B Tokyo Move\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ['B', 'Tokyo', 'Move'])

    def test_read_3(self):
        s = "C London Support\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ['C', 'London', 'Support'])   


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', 'Madrid')
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'C', 'Tokyo')
        self.assertEqual(w.getvalue(), "C Tokyo\n")
    
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'D', 'Berlin')
        self.assertEqual(w.getvalue(), "D Berlin\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, 'Y', '[dead]')
        self.assertEqual(w.getvalue(), "Y [dead]\n")
    

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Tokyo Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("X Oslo Hold\nY Dallas Move Oslo\nZ Austin Support X\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "X Oslo\nY [dead]\nZ Austin\n")

    def test_solve_3(self):
        r = StringIO("C Berlin Move Venice\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C Venice\n")

    def test_solve_4(self):
        r = StringIO("C Berlin Move Venice\nD Venice Hold\nE Rome Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C [dead]\nD [dead]\nE Rome\n")
    
    def test_solve_5(self):
        r = StringIO("C Berlin Move Venice\nD Venice Hold\nE Dallas Support C\nF Plano Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C [dead]\nD [dead]\nE Dallas\nF Plano\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Move HongKong\nB HongKong Hold\nC Dehli Support A\nD Mumbai Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A HongKong\nB [dead]\nC Dehli\nD Mumbai\n")

    def test_solve_7(self):
        r = StringIO("C London Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C London\n")
    
    def test_solve_8(self):
        r = StringIO("C London Move Paris\nD Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C [dead]\nD [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
