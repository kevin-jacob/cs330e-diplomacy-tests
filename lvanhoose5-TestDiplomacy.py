#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve
from Diplomacy import Army, diplomacy_solve, diplomacy_read, diplomacy_run, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # -----
    # read
    # -----

    def test_read_1(self):
        s = StringIO("A Houston Hold")
        result = diplomacy_read(s) # makes into list of army objects
        
        army = result[0]
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Houston")
        self.assertEqual(army.action, "Hold")
    
    def test_read_2(self):
        s = StringIO("A Houston Hold\nB Austin Move Houston\nC ElPaso Support B")
        result = diplomacy_read(s) # makes into list of army objects
        
        army = result[0]
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Houston")
        self.assertEqual(army.action, "Hold")
        
        army = result[1]
        self.assertEqual(army.name, "B")
        self.assertEqual(army.location, "Austin")
        self.assertEqual(army.action, "Move")
        self.assertEqual(army.arg, "Houston")
        
        army = result[2]
        self.assertEqual(army.name, "C")
        self.assertEqual(army.location, "ElPaso")
        self.assertEqual(army.action, "Support")
        self.assertEqual(army.arg, "B")

    def test_read_3(self):
        s = StringIO("A Houston Move SanAntonio\nB SanAntonio Move Houston")
        result = diplomacy_read(s) # makes into list of army objects
        
        army = result[0]
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Houston")
        self.assertEqual(army.action, "Move")
        self.assertEqual(army.arg, "SanAntonio")


        army = result[1]
        self.assertEqual(army.name, "B")
        self.assertEqual(army.location, "SanAntonio")
        self.assertEqual(army.action, "Move")
        self.assertEqual(army.arg, "Houston")

        
    # -----
    # print
    # -----
    
    def test_print_1(self):
        a = Army(name="A", location="Houston", action="Hold")
        b = Army(name="B", location="Houston", action="Hold")
        c = Army(name="C", location="ElPaso", action="Support", arg="B")

        a.alive = False

        results = [a,b,c]

        w = StringIO()

        diplomacy_print(results, w) # formats results
        
        self.assertEqual(w.getvalue(), 'A [dead]\nB Houston\nC ElPaso')

    def test_print_2(self):
        a = Army(name="A", location="Houston", action="Hold")
        b = Army(name="B", location="Houston", action="Hold")
        c = Army(name="C", location="ElPaso", action="Support", arg="B")

        b.alive = False
        c.alive = False

        results = [a,b,c]

        w = StringIO()
        diplomacy_print(results, w) # formats results
        self.assertEqual(w.getvalue(), 'A Houston\nB [dead]\nC [dead]')

    def test_print_3(self):
        a = Army(name="A", location="Houston", action="Hold")
        b = Army(name="B", location="Houston", action="Hold")

        a.alive = False
        b.alive = False

        results = [a,b]

        w = StringIO()
        diplomacy_print(results, w) # formats results    
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]')

    def test_print_4(self):
        a = Army(name="A", location="Houston", action="Hold")
        b = Army(name="B", location="Dallas", action="Hold")
        c = Army(name="C", location="ElPaso", action="Support", arg="B")
        results = [a,b,c]

        w = StringIO()
        diplomacy_print(results, w) # formats results
        self.assertEqual(w.getvalue(), 'A Houston\nB Dallas\nC ElPaso')


    # -----
    # run
    # -----
    
    def test_run_1(self):

        a = Army(name="A", location="Houston", action="Hold")
        armies = [a]

        # pass armies into run, output is armies after battle
        armies = diplomacy_run(armies) 
        self.assertEqual(armies[0].name, "A")
        self.assertEqual(armies[0].location, "Houston")
        self.assertEqual(armies[0].action, "Hold")
        self.assertEqual(armies[0].alive, True)
        self.assertEqual(armies[0].strength, 1)


    def test_run_2(self):

        a = Army(name="A", location="Houston", action="Hold")
        b = Army(name="B", location="Austin", action="Move", arg="Houston")
        c = Army(name="C", location="ElPaso", action="Support", arg="B")
        armies = [a,b,c]

        # pass armies into run, output is armies after battle
        armies = diplomacy_run(armies) 
        
        self.assertEqual(armies[0].name, "A")
        self.assertEqual(armies[0].location, "Houston")
        self.assertEqual(armies[0].action, "Hold")
        self.assertEqual(armies[0].alive, False)
        self.assertEqual(armies[0].strength, 1)
        
        self.assertEqual(armies[1].name, "B")
        self.assertEqual(armies[1].location, "Houston")
        self.assertEqual(armies[1].action, "Move")
        self.assertEqual(armies[1].alive, True)
        self.assertEqual(armies[1].strength, 2)
        
        self.assertEqual(armies[2].name, "C")
        self.assertEqual(armies[2].location, "ElPaso")
        self.assertEqual(armies[2].action, "Support")
        self.assertEqual(armies[2].alive, True)
        self.assertEqual(armies[2].strength, 1)
        
    def test_run_3(self):

        a = Army(name="A", location="Houston", action="Move", arg="ElPaso")
        b = Army(name="B", location="Austin", action="Move", arg="ElPaso")
        c = Army(name="C", location="Dallas", action="Move", arg="ElPaso")

        
        armies = [a,b,c]
        # pass armies into run, output is armies after battle
        armies = diplomacy_run(armies) 
        
        self.assertEqual(armies[0].name, "A")
        self.assertEqual(armies[0].location, "ElPaso")
        self.assertEqual(armies[0].action, "Move")
        self.assertEqual(armies[0].alive, False)
        self.assertEqual(armies[0].strength, 1)
        
        self.assertEqual(armies[1].name, "B")
        self.assertEqual(armies[1].location, "ElPaso")
        self.assertEqual(armies[1].action, "Move")
        self.assertEqual(armies[1].alive, False)
        self.assertEqual(armies[1].strength, 1)
        
        self.assertEqual(armies[2].name, "C")
        self.assertEqual(armies[2].location, "ElPaso")
        self.assertEqual(armies[2].action, "Move")
        self.assertEqual(armies[2].alive, False)
        self.assertEqual(armies[2].strength, 1)
        
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Houston Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Houston")

    def test_solve_2(self):
        r = StringIO("A Houston Hold\nB Austin Move Houston\nC ElPaso Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Houston\nC ElPaso")

    def test_solve_3(self):
        r = StringIO("A Houston Hold\nB Austin Move Houston\nC ElPaso Move Houston\nD Dallas Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Dallas\nE Austin")
    
    def test_solve_4(self):
        r = StringIO("A Houston Move ElPaso\nB Dallas Move ElPaso\nC Austin Move ElPaso\nD FortWorth Move ElPaso")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")
        
    def test_solve_5(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "")
        
    def test_solve_6(self):
        r = StringIO("A Houston Move Austin\nB Austin Move SanAntonio\nC SanAntonio Move Houston")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Austin\nB SanAntonio\nC Houston")
        
    def test_solve_7(self):
        r = StringIO("A Houston Hold\nB Austin Support A\nC SanAntonio Move Austin")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\nC [dead]")  


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
