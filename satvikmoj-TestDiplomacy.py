#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_solve, print_status

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # return validity/failure
    # ----

    def test_returnValidity(self):
        inputs = ["A Madrid Hold", 'B London Hold']
        outputs = [1]
        v = diplomacy_eval(inputs)
        assert type(v) == list

    def test_failure(self):
        inputs = []
        outputs = [1]
        v = diplomacy_eval(inputs)
        try:
            self.assertEqual(v, outputs)
            print("Assert Equal Not Working")
        except AssertionError:
            print("success")


    def test_eval_1(self):
        inputs = ['A Madrid Hold', 'B Barcelona Move Madrid',
                  'C London Move Madrid', 'D Paris Support B']
        outputs = ['A [dead]', 'B Madrid', 'C [dead]', 'D Paris']
        v = diplomacy_eval(inputs)
        self.assertEqual(v, outputs)

    def test_eval_2(self):
        inputs = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B']
        outputs = ['A [dead]', 'B Madrid', 'C London']
        v = diplomacy_eval(inputs)
        self.assertEqual(v, outputs)

    def test_eval_3(self):
        inputs = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London']
        outputs = ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]']
        v = diplomacy_eval(inputs)
        self.assertEqual(v, outputs)

    def test_eval_4(self):
        inputs = ['A Madrid Hold', 'B Barcelona Move Madrid',
                  'C London Move Madrid', 'D Paris Support B',
                  'E Austin Support A']
        outputs = ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin']
        v = diplomacy_eval(inputs)
        self.assertEqual(v, outputs)

    # # -----
    # # solve
    # # -----

    def test_solve1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_solve3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    #
    #
    #
    #
    # # -----
    # # print
    # # -----
    #
    #

    def test_print(self):
        outputs = ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin']
        w = StringIO()
        print_status(outputs, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")



# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
