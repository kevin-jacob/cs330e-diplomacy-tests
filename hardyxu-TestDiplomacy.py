#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Hardy
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, def_army, clean_data

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, m, n = diplomacy_read(s)
        self.assertEqual(i,  'A')
        self.assertEqual(j, 'Madrid')
        self.assertEqual(m,  'Hold')
        self.assertEqual(n, '')

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j, m, n = diplomacy_read(s)
        self.assertEqual(i,  'B')
        self.assertEqual(j, 'Barcelona')
        self.assertEqual(m,  'Move')
        self.assertEqual(n, 'Madrid')

    def test_read_3(self):
        s = "C London Support B\n"
        i, j, m, n = diplomacy_read(s)
        self.assertEqual(i,  'C')
        self.assertEqual(j, 'London')
        self.assertEqual(m,  'Support')
        self.assertEqual(n, 'B')

    # ----
    # eval
    # ----

    def test_eval_1(self):
        armies = []
        clean_data()
        armies.append(def_army("A", "Madrid", "Hold", ""))
        armies.append(def_army("B", "Barcelona", "Move", "Madrid"))
        armies = diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "[dead]")

    def test_eval_2(self):
        armies = []
        clean_data()
        armies.append(def_army("A", "Madrid", "Hold", ""))
        armies.append(def_army("B", "Barcelona", "Move", "Madrid"))
        armies.append(def_army("C", "London", "Support", "B"))
        armies = diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "Madrid")
        self.assertEqual(armies[2].location, "London")

    def test_eval_3(self):
        armies = []
        clean_data()
        armies.append(def_army("A", "Madrid", "Hold", ""))
        armies.append(def_army("B", "Barcelona", "Move", "Madrid"))
        armies.append(def_army("C", "London", "Support", "B"))
        armies.append(def_army("D", "Austin", "Move", "London"))
        armies = diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "[dead]")
        self.assertEqual(armies[2].location, "[dead]")
        self.assertEqual(armies[3].location, "[dead]")


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', '[dead]')
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'B', 'Madrid')
        self.assertEqual(w.getvalue(), "B Madrid\n")


    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'C', 'London')
        self.assertEqual(w.getvalue(), "C London\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")


    def test_solve_2(self):
        r = StringIO("\nA Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")


    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\n")




# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover


$ cat TestDiplomacy.out
.............
----------------------------------------------------------------------
Ran 13 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          70      0     28      0   100%
TestDiplomacy.py      89      0      0      0   100%
--------------------------------------------------------------
TOTAL                159      0     28      0   100%
"""
