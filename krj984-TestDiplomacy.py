#!/usr/bin/env python3

# -------------------------------
# Kevin Jacob and Ryan Brown
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_input, diplomacy_solve, battlefield, results, supports, diplomacy_game

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Nigeria Hold\n"
        t = read_input(s)
        self.assertEqual(t,  [['A', 'Nigeria', 'Move', 'Nigeria']])

    def test_read_2(self):
        s = "B Singapore Move SriLanka\n"
        t = read_input(s)
        self.assertEqual(t,  [['B', 'Singapore', 'Move', 'SriLanka']])

    def test_read_3(self):
        s = "A Lagos Hold\nB Nairobi Move Lagos\nC Dakar Support A\nD Cairo Hold"
        t = read_input(s)
        self.assertEqual(t,  [['A', 'Lagos', 'Move', 'Lagos'], ['B', 'Nairobi', 'Move', 'Lagos'], ['C', 'Dakar', 'Support', 'A'], ['D', 'Cairo', 'Move', 'Cairo']])

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid")

    def test_solve_2(self):
        r = StringIO("\nA Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]")


    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London")

    # --------------
    # diplomacy game
    # --------------

    def test_game_1(self):
        t = diplomacy_game([['A', 'Lagos', 'Move', 'Lagos'], ['B', 'Nairobi', 'Move', 'Lagos'], ['C', 'Dakar', 'Support', 'A'], ['D', 'Cairo', 'Move', 'Cairo']])
        self.assertEqual(t,[{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Dakar', 'Action': 'Support', 'Units': 1, 'Supporting': 'A'}, {'Army': 'D', 'Location': 'Cairo', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    def test_game_2(self):
        t = diplomacy_game([['A', 'Nigeria', 'Move', 'Nigeria']])
        self.assertEqual(t, [{'Army': 'A', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    def test_game_3(self):
        t = diplomacy_game([['A', 'Nigeria', 'Move', 'Lagos'], ['B', 'Lagos', 'Move', 'Lagos'], ['C', 'Dakar', 'Support', 'B'], ['D', 'Cairo', 'Move', 'Lagos']])
        self.assertEqual(t,[{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Dakar', 'Action': 'Support', 'Units': 1, 'Supporting': 'B'}, {'Army': 'D', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    # --------
    # Supports
    # --------
    
    def test_support_1(self):
        t = supports([{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Dakar', 'Action': 'Support', 'Units': 1, 'Supporting': 'B'}, {'Army': 'D', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}]) 
        self.assertEqual(t,[{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Lagos', 'Action': 'Move', 'Units': 2, 'Supporting': ''}, {'Army': 'C', 'Location': 'Dakar', 'Action': 'Support', 'Units': 1, 'Supporting': 'B'}, {'Army': 'D', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    def test_support_2(self):
        t = supports([{'Army': 'A', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, [{'Army': 'A', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
    
    def test_support_3(self):
        t = supports([{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'D', 'Location': 'Cairo', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, [{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'D', 'Location': 'Cairo', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    def test_support_4(self):
        t = supports([{'Army': 'A', 'Location': 'Steve2', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Steve2', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Charly', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'D', 'Location': 'Charly', 'Action': 'Support', 'Units': 1, 'Supporting': 'A'}, {'Army': 'E', 'Location': 'Wilson', 'Action': 'Support', 'Units': 1, 'Supporting': 'A'}, {'Army': 'F', 'Location': 'Wilson', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, [{'Army': 'A', 'Location': 'Steve2', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Steve2', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Charly', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'D', 'Location': 'Charly', 'Action': 'Support', 'Units': 1, 'Supporting': ''}, {'Army': 'E', 'Location': 'Wilson', 'Action': 'Support', 'Units': 1, 'Supporting': ''}, {'Army': 'F', 'Location': 'Wilson', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])

    # -----------
    # Battlefield
    # -----------

    def test_battlefield_1(self):
        t = battlefield([{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'C', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'D', 'Location': 'Cairo', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, {'Lagos': [('A', 1)], 'Cairo': [('D', 1)], 'Nigeria': [('B', 1), ('C', 1)]})

    def test_battlefield_2(self):
        t = battlefield([{'Army': 'A', 'Location': 'Nigeria', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, {'Nigeria': [('A', 1)]})

    def test_battlefield_3(self):
        t = battlefield([{'Army': 'A', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}, {'Army': 'B', 'Location': 'Lagos', 'Action': 'Move', 'Units': 2, 'Supporting': ''}, {'Army': 'C', 'Location': 'Dakar', 'Action': 'Support', 'Units': 1, 'Supporting': 'B'}, {'Army': 'D', 'Location': 'Lagos', 'Action': 'Move', 'Units': 1, 'Supporting': ''}])
        self.assertEqual(t, {'Dakar': [('C', 1)], 'Lagos': [('A', 1), ('B', 2), ('D', 1)]})

    # -------
    # Results
    # -------

    def test_results_1(self):
        t = results({'Dakar': [('C', 1)], 'Lagos': [('A', 1), ('B', 2), ('D', 1)]})
        self.assertEqual(t, {'A': '[dead]', 'B': 'Lagos', 'C': 'Dakar', 'D': '[dead]'})

    def test_results_2(self):
        t = results({'Paris': [('D', 2), ('F', 1)], 'Ontario': [('E', 1)], 'Madrid': [('A', 1), ('B', 1), ('C', 1)]})
        self.assertEqual(t, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Ontario', 'F': '[dead]'})

    def test_results_3(self):
        t = results({'Steve2': [('A', 3), ('B', 2), ('C', 1)], 'Charly': [('D', 1)], 'Wilson': [('E', 1)], 'Steve': [('F', 1)]})
        self.assertEqual(t, {'A': 'Steve2', 'B': '[dead]', 'C': '[dead]', 'D': 'Charly', 'E': 'Wilson', 'F': 'Steve'})

# ----
# main
# ----

if __name__ == "__main__":
    main()
