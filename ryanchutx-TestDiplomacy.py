from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve,diplomacy_solver

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):

    def test_solve_1(self):
        v = diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid",
                            "C London Move Madrid", "D Paris Support B", "E Dublin Support D"])
        self.assertEqual(v, ["A [dead]", "B Madrid",
                         "C [dead]", "D Paris", "E Dublin"])

    def test_solve_2(self):
        v = diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid",
                            "D Paris Support B", "E Dublin Support D", "F Dallas Move Paris"])
        self.assertEqual(v, ["A [dead]", "B [dead]",
                         "C [dead]", "D Paris", "E Dublin", "F [dead]"])

    def test_solve_3(self):
        v = diplomacy_solve(
            ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]"])

    def test_solve_4(self):
        v = diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid"])
        self.assertEqual(v, ["A [dead]", "B [dead]"])

    def test_solve_5(self):
        v = diplomacy_solve(["A Madrid Hold", "B Barcelona Move Madrid",
                            "C London Move Madrid", "D Paris Support B", "E Dublin Support D", "F Beijing Support E"])
        self.assertEqual(v, ["A [dead]", "B Madrid",
                         "C [dead]", "D Paris", "E Dublin", "F Beijing"])
                         
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Beijing Support E\n")
        w = StringIO()
        diplomacy_solver(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\nF Beijing\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
coverage report -m                      >> TestDiplomacy.out
cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          78      0     62      0   100%
TestDiplomacy.py      26      0      0      0   100%
--------------------------------------------------------------
TOTAL                104      0     62      0   100%
"""
