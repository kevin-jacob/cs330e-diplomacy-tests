from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_result, diplomacy_print, diplomacy_setup, diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):
    
    # -----
    # setup
    # -----

    def test_setup1(self):
        s = 'A London Hold'
        sub = diplomacy_setup(s)
        self.assertEqual(sub, ({'A':{'city': 'London', 'act':'Hold', 'supporters':[]}}, 
                                [], 
                                [],
                                {'London': ['A']}))

    def test_setup2(self):
        s = 'A London Move Madrid\nB Madrid Move London'
        sub = diplomacy_setup(s)
        self.assertEqual(sub, ({'A': {'city': 'London', 'act':'Move', 'supporters':[], 'move':'Madrid'},
                                'B': {'city': 'Madrid', 'act':'Move', 'supporters':[], 'move':'London'}}, 
                                ['A','B'], 
                                [],
                                {'London': ['A'], 'Madrid': ['B']}))

    def test_setup3(self):
        s = 'A London Move Madrid\nB Madrid Move London\nC NewYork Support B'
        sub = diplomacy_setup(s)
        self.assertEqual(sub, ({'A': {'city': 'London', 'act': 'Move', 'supporters': [], 'move': 'Madrid'},
                                'B': {'city': 'Madrid', 'act': 'Move', 'supporters': [], 'move': 'London'}, 
                                'C': {'city': 'NewYork', 'act': 'Support', 'supporters': [], 'support': 'B'}}, 
                                ['A', 'B'], 
                                ['C'], 
                                {'London': ['A'], 'Madrid': ['B'], 'NewYork': ['C']}))

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A London Move Madrid\nB Madrid Move London\nC NewYork Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 
                        'A Madrid\nB London\nC NewYork')

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 
                        'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin')

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 
                        'A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin')

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 
                        'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]')
        
if __name__ == "__main__":
    main()