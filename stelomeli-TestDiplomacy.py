#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, get_winner, times_in_dict

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):

    # -----
    # times_in_dict
    # -----
    def test_times_in_dict_1(self):
        army = 'A'
        dict = {'B':'A', 'C':'D'}
        count = times_in_dict(army, dict)
        self.assertEqual(
            count, 1
        )

    def test_times_in_dict_2(self):
        army = 'C'
        dict = {'B':'A', 'C':'D'}
        count = times_in_dict(army, dict)
        self.assertEqual(
            count, 0
        )

    def test_times_in_dict_3(self):
        army = 'A'
        dict = {'B':'A', 'C':'A', 'E':'A'}
        count = times_in_dict(army, dict)
        self.assertEqual(
            count, 3
        )

    # -----
    # get_winner
    # -----
    def test_get_winner_1(self):
        army = 'A'
        support = {'B':'A'}
        winner = get_winner(army, support)
        self.assertEqual(
            winner, 'A'
        )
    
    def test_get_winner_2(self):
        army = ''
        support = {'B':'A'}
        winner = get_winner(army, support)
        self.assertEqual(
            winner, None
        )
    def test_get_winner_3(self):
        army = 'AC'
        support = {'B':'A', 'D':'A', 'E':'C'}
        winner = get_winner(army, support)
        self.assertEqual(
            winner, 'A'
        )
    
    def test_get_winner_4(self):
        army = 'AC'
        support = {'B':'A', 'D':'A', 'E':'C', 'F':'C'}
        winner = get_winner(army, support)
        self.assertEqual(
            winner, None
        )

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma no coverage
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
..........
----------------------------------------------------------------------
Ran 10 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          78      0     46      0   100%
TestDiplomacy.py      56      0      0      0   100%
--------------------------------------------------------------
TOTAL                134      0     46      0   100%
"""
