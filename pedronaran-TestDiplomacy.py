from unittest import main, TestCase
from Diplomacy import diplomacy_solve, Army

class TestDiplomacy(TestCase):
    def test_solve_1(self):
        output = list(diplomacy_solve('A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'))
        expected = ['A [dead]', 'B Madrid', 'C London']
        self.assertEqual(output, expected)

    def test_solve_2(self):
        output = list(diplomacy_solve('A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B', 'D Austin Move London'))
        expected = ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]']
        self.assertEqual(output, expected)

    def test_solve_3(self):
        output = list(diplomacy_solve('A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'))
        expected = ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin']
        self.assertEqual(output, expected)

    def test_solve_4(self):
        output = list(diplomacy_solve('A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A', 'F Dallas Move Austin'))
        expected = ['A [dead]', 'B Madrid', 'C [dead]', 'D Paris', 'E [dead]', 'F [dead]']
        self.assertEqual(output, expected)

if __name__ == "__main__":
    main()