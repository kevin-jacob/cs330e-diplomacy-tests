#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

# from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
from Diplomacy import diplomacy_solve
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    # def test_read_1(self):
    #     s = "A Madrid Hold\n"
    #     i, j, v = diplomacy_read(s)
    #     self.assertEqual(i, "A")
    #     self.assertEqual(j, "Madrid")
    #     self.assertEqual(v, "Hold")

    # def test_read_2(self):
    #     s = "F Dallas Move Paris\n"
    #     i, j, v, k = diplomacy_read(s)
    #     self.assertEqual(i, "F")
    #     self.assertEqual(j, "Dallas")
    #     self.assertEqual(v, "Move")
    #     self.assertEqual(k, "Paris")

    # def test_read_3(self):
    #     s = "C London Move Madrid\n"
    #     i, j, v, k = diplomacy_read(s)
    #     self.assertEqual(i, "C")
    #     self.assertEqual(j, "London")
    #     self.assertEqual(v, "Move")
    #     self.assertEqual(k, "Madrid")

    # ----
    # eval
    # ----

    # def test_eval_1(self):
    #     s = "A Madrid Hold\n"
    #     v = diplomacy_eval(s)
    #     self.assertEqual(v, "A Madrid\n")

    # def test_eval_2(self):
    #     s = "A Madrid Hold\nB Barcelona Move Madrid"
    #     v = diplomacy_eval(s)
    #     self.assertEqual(v, "A [dead]\nB [dead]")

    # def test_eval_3(self):
    #     s = "A Madrid Hold\nB Barcelona Move Madrid\nC Austin Support A"
    #     v = diplomacy_eval(s)
    #     self.assertEqual(v, "A Madrid\nB [dead]\nC Austin")

    # def test_eval_4(self):
    #     s = "A Madrid Hold\nB Barcelona Move Madrid\nC Austin Move Barcelona"
    #     v = diplomacy_eval(s)
    #     self.assertEqual(v, "A [dead]\nB [dead]\nC Austin")

    # -----
    # print
    # -----

    # def test_print_1(self):
    #     w = StringIO()
    #     diplomacy_print(w, "A", "Madrid")
    #     self.assertEqual(w.getvalue(), "A Madrid\n")

    # def test_print_2(self):
    #     w = StringIO()
    #     diplomacy_print(w, "C", "[dead]")
    #     self.assertEqual(w.getvalue(), "C [dead]\n")

    # def test_print_3(self):
    #     w = StringIO()
    #     diplomacy_print(w, "E", "Dublin")
    #     self.assertEqual(w.getvalue(), "E Dublin\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Austin Move Madrid\nC London Support A\nD Houston Support B\nE Barcelona Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Houston\nE Barcelona\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Austin Move Madrid\nC London Support A\nD Houston Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Houston\n")
    

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
